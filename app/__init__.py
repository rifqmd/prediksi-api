from flask import Flask
from app import routes

# Untuk menjelaskan nama modul yang digunakan, sehingga ketika folder lain memanggil folder app akan otomatis teridentifikasi.
app = Flask(__name__)